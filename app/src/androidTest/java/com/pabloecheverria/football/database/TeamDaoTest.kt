package com.pabloecheverria.football.database

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.pabloecheverria.football.base.database.FootballDatabase
import com.pabloecheverria.football.feature.teams.db.Team
import com.pabloecheverria.football.feature.teams.db.TeamDao
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.io.IOException

class TeamDaoTest {

    private lateinit var database: FootballDatabase
    private lateinit var teamDao: TeamDao

    @Before
    fun setup() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            FootballDatabase::class.java
        ).build()
        teamDao = database.teamDao()
    }

    @Test
    fun insertCorrectly() = runBlocking {
        val team = Team(teamsId = "1", name = "River Plate", badgeUrl = "http.image.url", leagueId = leagueId)
        teamDao.addTeam(team)
        val teams = teamDao.getTeams(leagueId)
        assertThat(teams.first().size, equalTo(1))
        assertThat(teams.first()[0].name, equalTo("River Plate"))
        assertThat(teams.first()[0].teamsId, equalTo("1"))
        assertThat(teams.first()[0].badgeUrl, equalTo("http.image.url"))
    }

    @Test
    fun updateCorrectly() = runBlocking {
        val teamOriginal = Team(teamsId = "1", name = "River Plate", badgeUrl = "http.image.url", leagueId = leagueId)
        val teamExpected =
            Team(teamsId = "1", name = "River Plate plus", badgeUrl = "http.image.url plus", leagueId = leagueId)
        teamDao.addTeam(teamOriginal)
        teamDao.updateTeam(teamExpected)
        val teams = teamDao.getTeams(leagueId)
        assertThat(teams.first().size, equalTo(1))
        assertThat(teams.first()[0].name, equalTo("River Plate plus"))
        assertThat(teams.first()[0].teamsId, equalTo("1"))
        assertThat(teams.first()[0].badgeUrl, equalTo("http.image.url plus"))
    }

    @Test
    fun deleteCorrectly() = runBlocking {
        val teamOriginal = Team(teamsId = "1", name = "River Plate", badgeUrl = "http.image.url", leagueId = leagueId)
        teamDao.addTeam(teamOriginal)
        teamDao.deleteTeam(teamOriginal)
        val teams = teamDao.getTeams(leagueId)
        assertThat(teams.first().size, equalTo(0))
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        database.close()
    }

    companion object {
        private const val leagueId = 1
    }
}
