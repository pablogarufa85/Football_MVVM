package com.pabloecheverria.football.feature

import android.content.Intent
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.withDecorView
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.pabloecheverria.football.R
import com.pabloecheverria.football.base.custom.EspressoIdlingResource.getIdlingResource
import com.pabloecheverria.football.base.di.BASE_URL
import com.pabloecheverria.football.feature.teams.TeamsActivity
import com.pabloecheverria.football.util.CustomMatchers.atPosition
import com.pabloecheverria.football.util.CustomMatchers.hasItemCount
import com.pabloecheverria.football.util.CustomMatchers.isRefreshing
import com.pabloecheverria.football.util.CustomMatchers.withCollapsibleToolbarTitle
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.net.HttpURLConnection.HTTP_BAD_REQUEST
import java.net.HttpURLConnection.HTTP_OK
import java.util.concurrent.TimeUnit

@RunWith(AndroidJUnit4::class)
class TeamsActivityTest {

    private lateinit var mockWebServer: MockWebServer

    @get:Rule
    val activityTestRule = ActivityTestRule(TeamsActivity::class.java, true, false)

    @Before
    fun setUp() {
        // Startup mock web server
        mockWebServer = MockWebServer()
        mockWebServer.start(8080)

        BASE_URL = "http://localhost:8080/"

        // For Idling resources
        IdlingRegistry.getInstance().register(getIdlingResource())
    }


    @Test
    fun `whenServerRequestIsSuccessfulShouldShowListOfProducts`() {

        val dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                val mockedResponse =
                    InstrumentationRegistry.getInstrumentation().context.assets.open("json/response_teams_ok.json")
                        .bufferedReader().use { it.readText() }
                return MockResponse().setResponseCode(HTTP_OK).setBody(mockedResponse)
                    .setBodyDelay(FAKE_LOADING_TIME_IN_MILLIS, TimeUnit.MILLISECONDS)
            }
        }

        mockWebServer.dispatcher = dispatcher

        val intent = Intent().apply {
            putExtra(TeamsActivity.EXTRA_LEAGUE_ID, 1)
            putExtra(TeamsActivity.EXTRA_LEAGUE_TITLE, "Everton")
            putExtra(TeamsActivity.EXTRA_LEAGUE_BADGE_URL, "www.image.com")
        }

        activityTestRule.launchActivity(intent)

        onView(withId(R.id.swipe_refresh)).check(matches(isRefreshing()))

        onView(withId(R.id.collapsing_tollbar)).check(matches(isDisplayed()))
        onView(withId(R.id.collapsing_tollbar)).check(matches(withCollapsibleToolbarTitle(`is`("Everton"))))

        onView(withId(R.id.recycler_teams)).check(matches(isDisplayed()))
        onView(withId(R.id.recycler_teams)).check(matches(hasItemCount(10)))
        onView(withId(R.id.recycler_teams)).check(
            matches(atPosition(0, hasDescendant(withText("Bylis"))))
        )

        onView(withId(R.id.swipe_refresh)).check(matches(not(isRefreshing())))
    }


    @Test
    fun `whenServerRequestHasFailedShouldShowToast`() {

        val dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse =
                MockResponse().setResponseCode(HTTP_BAD_REQUEST).setBody("empty body")
                    .setBodyDelay(FAKE_LOADING_TIME_IN_MILLIS, TimeUnit.MILLISECONDS)
        }
        mockWebServer.dispatcher = dispatcher

        val intent = Intent().apply {
            putExtra(TeamsActivity.EXTRA_LEAGUE_ID, 1)
            putExtra(TeamsActivity.EXTRA_LEAGUE_TITLE, "Everton")
            putExtra(TeamsActivity.EXTRA_LEAGUE_BADGE_URL, "www.image.com")
        }

        activityTestRule.launchActivity(intent)

        onView(withId(R.id.swipe_refresh)).check(matches(isRefreshing()))

        onView(withId(R.id.swipe_refresh)).check(matches(not(isRefreshing())))
        onView(withId(R.id.recycler_teams)).check(matches(isDisplayed()))
        onView(withId(R.id.recycler_teams)).check(matches(hasItemCount(0)))

        onView(withText(R.string.error_message))
            .inRoot(withDecorView(not(activityTestRule.activity.window.decorView)))
            .check(matches(isDisplayed()))
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()

        // For Idling resource
        IdlingRegistry.getInstance().unregister(getIdlingResource())
    }

    companion object {
        private const val FAKE_LOADING_TIME_IN_MILLIS = 750L
    }
}
