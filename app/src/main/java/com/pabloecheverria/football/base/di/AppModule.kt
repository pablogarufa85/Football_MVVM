package com.pabloecheverria.football.base.di

import android.content.Context
import androidx.room.Room
import com.google.firebase.firestore.FirebaseFirestore
import com.pabloecheverria.football.base.database.FootballDatabase
import com.pabloecheverria.football.base.mapper.Mapper
import com.pabloecheverria.football.base.network.FirestoreDataSource
import com.pabloecheverria.football.base.network.FootballService
import com.pabloecheverria.football.feature.leagues.LeagueRepository
import com.pabloecheverria.football.feature.leagues.LeagueRepositoryImpl
import com.pabloecheverria.football.feature.leagues.LeaguesActivityViewModel
import com.pabloecheverria.football.feature.leagues.db.League
import com.pabloecheverria.football.feature.leagues.db.LeagueDao
import com.pabloecheverria.football.feature.leagues.db.LeagueMapper
import com.pabloecheverria.football.feature.leagues.network.LeagueDTO
import com.pabloecheverria.football.feature.teams.TeamMapper
import com.pabloecheverria.football.feature.teams.TeamRepository
import com.pabloecheverria.football.feature.teams.TeamRepositoryImpl
import com.pabloecheverria.football.feature.teams.TeamsActivityViewModel
import com.pabloecheverria.football.feature.teams.db.Team
import com.pabloecheverria.football.feature.teams.db.TeamDao
import com.pabloecheverria.football.feature.teams.network.TeamDTO
import kotlinx.coroutines.Dispatchers.IO
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


val myModule: Module = module {

    viewModel { (leagueId: Int) -> TeamsActivityViewModel(leagueId, get(), get()) }

    viewModel { LeaguesActivityViewModel(get(), get()) }

    single(named("team_mapper")) { TeamMapper() as Mapper<TeamDTO, Team> }

    single(named("league_mapper")) { LeagueMapper() as Mapper<LeagueDTO, League> }

    single { IO }
}

val dataModule: Module = module {

    single<TeamRepository> { TeamRepositoryImpl(get(named("team_mapper")), get(), get(), get()) }

    single<LeagueRepository> {
        LeagueRepositoryImpl(
            get(named("league_mapper")),
            get(),
            get(),
            get()
        )
    }

    single { provideTeamDao(androidContext()) }

    single { provideLeagueDao(androidContext()) }

    single { FootballService(provideRetrofit()) }

    single { FirestoreDataSource(get()) }

    single<FirebaseFirestore> { FirebaseFirestore.getInstance() }
}

private fun provideRetrofit(): Retrofit {
    return Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(OkHttpClient())
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}

private fun provideTeamDao(context: Context): TeamDao = provideFootballDatabase(context).teamDao()

private fun provideLeagueDao(context: Context): LeagueDao =
    provideFootballDatabase(context).leagueDao()

private fun provideFootballDatabase(context: Context) =
    Room.databaseBuilder(context, FootballDatabase::class.java, "database-teams").build()

var BASE_URL = "https://apiv2.apifootball.com"

