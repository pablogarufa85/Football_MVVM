package com.pabloecheverria.football.base.custom

sealed class State<T>(var data: T? = null) {
    class Success<T>(data: T) : State<T>(data)
    class Loading<T>(data: T? = null) : State<T>(data)
    sealed class Error<T>(data: T? = null) : State<T>(data) {
        class NetworkError<T>(data: T? = null) : Error<T>(data)
        class GeneralError<T>(data: T? = null) : Error<T>(data)
    }

    override fun toString(): String {
        return when (this) {
            is Success -> "Success"
            is Error.GeneralError -> "General Error"
            is Error.NetworkError -> "Network Error"
            is Loading -> "Loading"
        }
    }
}


