package com.pabloecheverria.football.base.mapper

interface Mapper<I, O> {

    fun map(input: I): O

    fun mapList(input: List<I>): List<O> = input.map {
        map(it)
    }
}