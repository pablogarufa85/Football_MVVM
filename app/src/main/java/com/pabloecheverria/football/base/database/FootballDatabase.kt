package com.pabloecheverria.football.base.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.pabloecheverria.football.feature.leagues.db.League
import com.pabloecheverria.football.feature.leagues.db.LeagueDao
import com.pabloecheverria.football.feature.teams.db.Team
import com.pabloecheverria.football.feature.teams.db.TeamDao

@Database(entities = [Team::class, League::class], version = 1)
abstract class FootballDatabase : RoomDatabase() {
    abstract fun teamDao(): TeamDao
    abstract fun leagueDao(): LeagueDao
}