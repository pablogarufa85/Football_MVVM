package com.pabloecheverria.football.base.network

import arrow.core.Either
import arrow.core.Left
import arrow.core.Right
import com.google.firebase.firestore.FirebaseFirestore
import com.pabloecheverria.football.base.custom.State.Error
import com.pabloecheverria.football.feature.leagues.db.League
import com.pabloecheverria.football.feature.teams.db.Team
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


class FirestoreDataSource(private val firestoreDatabase: FirebaseFirestore) {

    suspend fun getTeams(leagueId: Int): Either<Error<List<Team>>, List<Team>> =
        suspendCoroutine { continuation ->
            firestoreDatabase.collection(TABLE_TEAMS.format(leagueId)).get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val teams =
                            task.result?.map {
                                Team(
                                    teamsId = it.id,
                                    leagueId = leagueId,
                                    name = it.data[TEAMS_FIELD_NAME] as String,
                                    badgeUrl = it.data[TEAMS_FIELD_IMAGE] as String
                                )
                            } ?: emptyList()
                        continuation.resume(Right(teams))
                    } else {
                        continuation.resume(Left(Error.GeneralError()))
                    }
                }
        }

    suspend fun getLeagues(): Either<Error<List<League>>, List<League>> =
        suspendCoroutine { continuation ->
            firestoreDatabase.collection(TABLE_COMPETITIONS).get().addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val leagues =
                        task.result?.map {
                            League(
                                leagueId = it.id.toInt(),
                                name = it.data[LEAGUES_FIELD_NAME] as String,
                                badgeUrl = it.data[LEAGUES_FIELD_IMAGE] as String
                            )
                        } ?: emptyList()
                    continuation.resume(Right(leagues))
                } else {
                    continuation.resume(Left(Error.GeneralError()))
                }
            }
        }

    suspend fun addTeam(name: String, leagueId: Int): Either<String, Team> =
        suspendCoroutine { continuation ->
            val teamMap: MutableMap<String, Any> = HashMap()
            teamMap[TEAMS_FIELD_NAME] = name
            teamMap[TEAMS_FIELD_IMAGE] = ""

            firestoreDatabase.collection(TABLE_TEAMS.format(leagueId))
                .add(teamMap)
                .addOnSuccessListener {
                    continuation.resume(
                        Right(
                            Team(
                                teamsId = it.id,
                                name = name,
                                leagueId = leagueId
                            )
                        )
                    )
                }
                .addOnFailureListener { continuation.resume(Left(it.localizedMessage)) }
        }

    companion object {
        private const val TABLE_COMPETITIONS = "competitions"
        private const val TABLE_TEAMS = "competitions/%s/teams"
        private const val TEAMS_FIELD_NAME = "name"
        private const val TEAMS_FIELD_IMAGE = "image"
        private const val LEAGUES_FIELD_NAME = "name"
        private const val LEAGUES_FIELD_IMAGE = "image"
    }
}