package com.pabloecheverria.football.base.network

import arrow.core.Either
import arrow.core.Left
import arrow.core.Right
import com.pabloecheverria.football.base.custom.EspressoIdlingResource
import com.pabloecheverria.football.base.custom.State.Error
import com.pabloecheverria.football.feature.leagues.db.League
import com.pabloecheverria.football.feature.leagues.network.LeagueDTO
import com.pabloecheverria.football.feature.teams.db.Team
import com.pabloecheverria.football.feature.teams.network.TeamDTO
import retrofit2.Retrofit

class FootballService(private val retrofit: Retrofit) {

    private val service = lazy { retrofit.create(FootballApi::class.java) }

    suspend fun getTeams(leagueId: Int): Either<Error<List<Team>>, List<TeamDTO>> {

        EspressoIdlingResource.increment()
        return try {
            val response = service.value.getTeams(leagueId)
            EspressoIdlingResource.decrement()
            Right(response)
        } catch (exception: Exception) {
            EspressoIdlingResource.decrement()
            Left(Error.GeneralError())
        }
    }

    suspend fun getLeagues(): Either<Error<List<League>>, List<LeagueDTO>> {

        return try {
            val response = service.value.getLeagues()
            Right(response)
        } catch (exception: Exception) {
            Left(Error.GeneralError())
        }
    }
}