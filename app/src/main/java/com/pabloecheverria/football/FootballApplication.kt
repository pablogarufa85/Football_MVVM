package com.pabloecheverria.football

import android.app.Application
import com.pabloecheverria.football.base.di.dataModule
import com.pabloecheverria.football.base.di.myModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class FootballApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@FootballApplication)
            modules(myModule, dataModule)
        }
    }
}