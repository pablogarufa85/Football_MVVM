package com.pabloecheverria.football.feature.leagues.db

import com.pabloecheverria.football.base.mapper.Mapper
import com.pabloecheverria.football.feature.leagues.network.LeagueDTO

class LeagueMapper : Mapper<LeagueDTO, League> {

    override fun map(input: LeagueDTO): League {
        return League(leagueId = input.league_id, name = input.league_name, badgeUrl = input.league_logo)
    }
}