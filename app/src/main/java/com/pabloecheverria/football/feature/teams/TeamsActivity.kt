package com.pabloecheverria.football.feature.teams

import android.os.Bundle
import android.view.MenuItem
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.ItemTouchHelper.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.pabloecheverria.football.R
import com.pabloecheverria.football.base.custom.State
import com.pabloecheverria.football.base.extension.loadImageUrl
import com.pabloecheverria.football.base.extension.snack
import com.pabloecheverria.football.base.extension.toast
import com.pabloecheverria.football.feature.teams.db.Team
import kotlinx.android.synthetic.main.activity_teams.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class TeamsActivity : AppCompatActivity(), TeamListAdapter.Interaction {

    private lateinit var teamListAdapter: TeamListAdapter

    private val leagueId: Int by lazy {
        intent.extras?.get(EXTRA_LEAGUE_ID) as Int
    }

    private val mainFootballViewModel: TeamsActivityViewModel by viewModel { parametersOf(leagueId) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_teams)
        setupToolbar()
        initRecyclerView()
        setupSwipeRefresh()
        subscribeObservers()
        floatingActionButton.setOnClickListener {
            showDialogAddTeam()
        }
    }

    private fun setupToolbar() {
        setSupportActionBar(tool_bar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        collapsing_tollbar.title = intent.extras?.get(EXTRA_LEAGUE_TITLE) as String
        badge.loadImageUrl(intent.extras?.get(EXTRA_LEAGUE_BADGE_URL) as String)
    }

    private fun subscribeObservers() {
        lifecycleScope.launchWhenResumed {
            mainFootballViewModel.teams.observe(this@TeamsActivity, Observer {
                when (it) {
                    is State.Success -> handleSuccess(it.data)
                    is State.Error -> handleError(it.data)
                    is State.Loading -> handleLoading()
                }
            })
        }
    }

    private fun initRecyclerView() {
        recycler_teams.apply {
            layoutManager = LinearLayoutManager(this@TeamsActivity)
            teamListAdapter = TeamListAdapter(this@TeamsActivity)
            adapter = teamListAdapter
        }

        val callback: SimpleCallback =
            object : SimpleCallback(0, RIGHT or LEFT) {

                override fun onMove(
                    recyclerView: RecyclerView,
                    viewHolder: RecyclerView.ViewHolder,
                    target: RecyclerView.ViewHolder
                ): Boolean {
                    return false
                }

                override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                    val item = teamListAdapter.itemAt(viewHolder.adapterPosition)
                    val message = getString(R.string.deleted_message).format(item.name)
                    root_view.snack(
                        message = message,
                        f = { mainFootballViewModel.deleteTeam(item) })
                }
            }

        ItemTouchHelper(callback).attachToRecyclerView(recycler_teams)
    }

    private fun setupSwipeRefresh() {
        swipe_refresh.setOnRefreshListener {
            mainFootballViewModel.getTeams()
        }
    }

    private fun handleLoading() {
        swipe_refresh.isRefreshing = true
    }

    private fun handleSuccess(teams: List<Team>?) {
        swipe_refresh.isRefreshing = false
        renderTeams(teams)
    }

    private fun handleError(teams: List<Team>?) {
        swipe_refresh.isRefreshing = false
        renderTeams(teams)
        showErrorMessage()
    }

    private fun renderTeams(teams: List<Team>?) {
        teams?.let {
            teamListAdapter.submitList(it)
        }
    }

    private fun showErrorMessage() = toast(getString(R.string.error_message))

    override fun onItemSelected(position: Int, item: Team) {
    }

    private fun showDialogAddTeam() {
        MaterialAlertDialogBuilder(this)
            .setTitle(resources.getString(R.string.dialog_title))
            .setView(R.layout.dialog_entry)
            .setNegativeButton(resources.getString(R.string.dialog_decline)) { dialog, _ ->
                dialog.dismiss()
            }
            .setPositiveButton(resources.getString(R.string.dialog_accept)) { dialog, _ ->
                val editTeamName =
                    (dialog as? AlertDialog)?.findViewById<EditText>(R.id.dialog_team_input)
                val teamName = editTeamName?.text?.toString()
                teamName?.let {
                    addTeam(it)
                }
            }
            .show()
    }

    private fun addTeam(teamName: String) = mainFootballViewModel.addTeam(teamName)

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        const val EXTRA_LEAGUE_ID = "EXTRA_LEAGUE_ID"
        const val EXTRA_LEAGUE_TITLE = "EXTRA_EXTRA_LEAGUE_TITLE"
        const val EXTRA_LEAGUE_BADGE_URL = "EXTRA_LEAGUE_BADGE_URL"
    }
}
