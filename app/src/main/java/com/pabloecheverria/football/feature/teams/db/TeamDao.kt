package com.pabloecheverria.football.feature.teams.db

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface TeamDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addTeam(team: Team)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addTeams(teams: List<Team>)

    @Update
    suspend fun updateTeam(team: Team)

    @Update
    suspend fun updateTeams(teams: List<Team>)

    @Delete
    suspend fun deleteTeam(team: Team)

    @Query("SELECT * from teams WHERE leagueId=:leagueId ORDER BY name ASC")
    fun getTeams(leagueId: Int): Flow<List<Team>>

    @Query("DELETE FROM teams")
    suspend fun deleteAll()
}