package com.pabloecheverria.football.feature.leagues

import arrow.core.Either
import arrow.core.extensions.either.monad.flatMap
import com.pabloecheverria.football.base.custom.State
import com.pabloecheverria.football.base.mapper.Mapper
import com.pabloecheverria.football.base.network.FirestoreDataSource
import com.pabloecheverria.football.base.network.FootballService
import com.pabloecheverria.football.feature.leagues.db.League
import com.pabloecheverria.football.feature.leagues.db.LeagueDao
import com.pabloecheverria.football.feature.leagues.network.LeagueDTO
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.*

class LeagueRepositoryImpl(
    private val mapper: Mapper<LeagueDTO, League>,
    private val leagueDao: LeagueDao,
    private val footballService: FootballService,
    private val firestoreDataSource: FirestoreDataSource
) : LeagueRepository {

    override fun observeLeagues(): Flow<List<League>> {
        return leagueDao.getLeagues().filter { it.isNotEmpty() }
    }

    @ExperimentalCoroutinesApi
    override suspend fun getLeagues() = flow<State<List<League>>> {
        coroutineScope {
            val deferredApiResult = async { footballService.getLeagues() }
            val deferredFirestoreResult = async { firestoreDataSource.getLeagues() }

            var errorEmitted = false

            deferredApiResult.await()
                .fold({
                    errorEmitted = true
                    emit(it.apply { data = leagueDao.getLeagues().first() })
                }, {
                    leagueDao.addLeagues(mapper.mapList(it))
                })

            deferredFirestoreResult.await()
                .fold({
                    if (!errorEmitted) {
                        emit(it.apply { data = leagueDao.getLeagues().first() })
                    }
                }, {
                    leagueDao.addLeagues(it)
                })
        }

    }.onStart {
        emit(State.Loading())
    }.onEach {
        println("GARUFA Item emitted: $it")
    }
}