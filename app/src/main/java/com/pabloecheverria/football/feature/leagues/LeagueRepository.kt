package com.pabloecheverria.football.feature.leagues

import com.pabloecheverria.football.base.custom.State
import com.pabloecheverria.football.feature.leagues.db.League
import kotlinx.coroutines.flow.Flow

interface LeagueRepository {
    fun observeLeagues(): Flow<List<League>>
    suspend fun getLeagues(): Flow<State<List<League>>>
}