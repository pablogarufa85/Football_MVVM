package com.pabloecheverria.football.feature.teams.network

import com.google.gson.annotations.SerializedName

data class TeamsResponse(
    val teams: List<TeamDTO>
)

data class TeamDTO(
    @SerializedName("team_key") val team_team_key: Int,
    @SerializedName("team_name") val team_name: String,
    @SerializedName("team_badge") val team_badge: String
    //@SerializedName("players") val players: List<Players>,
    //@SerializedName("coaches") val coaches: List<Coaches>
)

data class Players(
    @SerializedName("player_key") val player_player_key: Int,
    @SerializedName("player_name") val player_name: String,
    @SerializedName("player_number") val player_number: Int,
    @SerializedName("player_country") val player_country: String,
    @SerializedName("player_type") val player_type: String,
    @SerializedName("player_age") val player_age: Int,
    @SerializedName("player_match_played") val player_match_played: Int,
    @SerializedName("player_goals") val player_goals: Int,
    @SerializedName("player_yellow_cards") val player_yellow_cards: Int,
    @SerializedName("player_red_cards") val player_red_cards: Int
)

data class Coaches(
    @SerializedName("coach_name") val coach_name: String,
    @SerializedName("coach_country") val coach_country: String,
    @SerializedName("coach_age") val coach_age: Int
)