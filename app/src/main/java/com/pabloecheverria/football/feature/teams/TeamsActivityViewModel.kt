package com.pabloecheverria.football.feature.teams

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pabloecheverria.football.base.custom.State
import com.pabloecheverria.football.feature.teams.db.Team
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TeamsActivityViewModel(
    private val leagueId: Int,
    private val repository: TeamRepository,
    private val dispatcher: CoroutineDispatcher
) : ViewModel() {

    private val _teams = MutableLiveData<State<List<Team>>>()
    val teams: LiveData<State<List<Team>>> = _teams

    init {
        observeTeams()
        getTeams()
    }

    private fun observeTeams() {
        viewModelScope.launch(dispatcher) {
            repository.observeTeams(leagueId).collect {
                withContext(Main) {
                    _teams.value = it
                }
            }
        }
    }

    fun getTeams() {
        viewModelScope.launch(dispatcher) {
            repository.getTeams(leagueId).collect {
                withContext(Main) {
                    _teams.value = it
                }
            }
        }
    }

    fun addTeam(teamName: String) {
        viewModelScope.launch(dispatcher) {
            repository.addTeam(teamName, leagueId)
        }
    }

    fun deleteTeam(team: Team) {
        viewModelScope.launch(dispatcher) {
            repository.deleteTeam(team)
        }
    }
}
